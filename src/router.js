import { createWebHistory, createRouter } from "vue-router";
import Table from "@/views/Table.vue";
import Charts from "@/views/Charts.vue";
import Maps from "@/views/Maps.vue";

const routes = [
  { 
    path: '/', 
    redirect: { name: 'Table' }
  },
  {
    path: "/table",
    name: "Table",
    component: Table,
  },
  {
    path: "/charts",
    name: "Charts",
    component: Charts,
  },
  {
    path: "/maps",
    name: "Maps",
    component: Maps,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;