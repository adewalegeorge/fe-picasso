import { createStore } from 'vuex'
import People from '@/people.json'

export default createStore({
  state: {
    persons: People
  },

  mutations: {
    UPDATE_PERSON (state, payload) {
      const personIndex = state.persons.findIndex(p => p._id === payload._id);
      state.persons[personIndex] = payload;
    },

    ADD_PERSON (state, payload) {
      state.persons.push(payload);
    }
  },

  actions: {
    updatePerson ({commit}, payload) {
      commit('UPDATE_PERSON', payload);
    },

    addPerson ({commit}, payload) {
      commit('ADD_PERSON', payload);
    }
  },

  getters: {
    eyeColors: state => { return [...new Set(state.persons.map(p => p.eyeColor))] },
    pets: state => { return [...new Set(state.persons.map(p => p.preferences.pet))] },
    fruits: state => { return [...new Set(state.persons.map(p => p.preferences.fruit))] },
  }
})