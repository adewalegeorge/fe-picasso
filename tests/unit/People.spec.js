
import { mount } from '@vue/test-utils'
import Person from '@/components/Person'
import { createStore } from 'vuex'

describe('Person', () => {
  const personData = {
    "_id": "5d5d7ad6b0e83bc2d9d67dfb",
    "age": 28,
    "eyeColor": "brown",
    "name": "Stephens Townsend",
    "gender": "male",
    "location": {
      "latitude": 26.723281,
      "longitude": 99.391104
    },
    "preferences": {
      "pet": "bird",
      "fruit": "apple"
    }
  };
  let mockedFunc, store;
  
  beforeEach(() => {
    mockedFunc = jest.fn()
    store = createStore({
      state: { persons: [personData] },
      actions: { updatePerson: mockedFunc },
      mutation: { UPDATE_PERSON: mockedFunc }
    })
  })

  test('render person', () => {
    const wrapper = mount(Person, {
      props: { person: store.state.persons[0] }
    })
    expect(wrapper.find('.c-person').exists()).toBe(true)
  })

  test('updatePerson dispatcher', async () =>  {
    const wrapper = mount(Person, {
      global: { plugins: [store] }
    })

    // console.log(wrapper);
    await wrapper.find('button.edit').trigger('click')
    await wrapper.find('input[name=name]').setValue('George Mason')
    await wrapper.find('button.save').trigger('click')
    expect(mockedFunc).toHaveBeenCalled()
  })
})

