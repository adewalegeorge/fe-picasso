const configSettings = {
  runtimeCompiler: true,
  devServer: {
    host: 'localhost'
  },
  pwa: {
    name: 'Frontend Picasso',
    themeColor: '#023761',
    msTileColor: '#023761',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
  },

  css: {
    sourceMap: true
  },
}

module.exports = configSettings;